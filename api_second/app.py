from flask import Flask
import requests

URL_API = "http://api_third:5003"
app = Flask(__name__)

@app.route("/")
def home():
    try:
        res = "2"
        response = requests.get(URL_API)
        if response.status_code == 200:
            res = res + "=>" + response.text
        else:
            res = res + "=>" + "api_third " + response.status_code
        return res
    except Exception as exc:
        return exc.args

if __name__ == "__main__":
    app.run()