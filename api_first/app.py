from flask import Flask
import requests

URL_API = "http://api_second:5002"
app = Flask(__name__)

@app.route("/")
def home():
    try:
        res = "3"
        response = requests.get(URL_API)
        if response.status_code == 200:
            res = res + "=>" + response.text
        else:
            res = res + "=>" + "api_second " + response.status_code
        return res
    except Exception as exc:
        return exc.args

if __name__ == "__main__":
    app.run()